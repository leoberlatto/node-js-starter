import express from 'express'
const app = express()
const port = 3000

import { config } from 'dotenv'

app.get('/', (req, res) => {
    config({ path: process.env.NODE_ENV === 'development' ? '.env.dev' : '.env' })

    res.send({ teste: true})
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})